<?php
/**
 * Created by PhpStorm.
 * User: evan
 * Date: 27/10/18
 * Time: 19:57
 */

class enfantDAO
{
    private $_db;

    public function __construct($db)
    {
        $this->setDb($db);
    }

    public function setDb(PDO $db){ $this->_db = $db; }

    public function add(enfant $enfant)
    {
        $q= $this->_db->prepare('INSERT INTO enfant(nom, prenom, passwd) VALUES(:nom, :prenom, :passwd)');

        $q->bindvalue(':nom', $enfant->nom());
        $q->bindvalue(':prenom', $enfant->prenom());
        $q->bindvalue(':passwd', $enfant->passwd());

        $q->execute();
    }

    public function get($id)
    {
        $id = (int) $id;

        $q = $this->_db->query('SELECT id, nom, prenom, passwd FROM enfant WHERE id= ' . $id);
        $donnees =$q->fetch(PDO::FETCH_ASSOC);

        return new enfant($donnees);

    }

    public function getList()
    {
        $enfants = [];
        $q = $this->_db->query('SELECT * FROM enfant');

        while ($donnees = $q->fetch(PDO::FETCH_ASSOC))
        {
            $enfants[] = new enfant($donnees);
        }
        return $enfants;
    }


}