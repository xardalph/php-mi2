<?php
/**
 * Created by PhpStorm.
 * User: evan
 * Date: 27/10/18
 * Time: 19:57
 */

class enfant
{
    private $_id;
    private $_nom;
    private $_prenom;
    private $_passwd;


    public function __construct(array $donnees)
    {
        foreach ($donnees as $key => $value)
        {
            // On récupère le nom du setter correspondant à l'attribut.
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method))
            {
                $this->$method($value);
            }

        }
    }

    public function id() { return $this->_id; }
    public function Nom() { return $this->_nom; }
    public function prenom() { return $this->_prenom; }
    public function passwd() { return $this->_passwd; }

    public function setId($id): void
    {
        $this->_id = (int) $id;
    }

    public function setNom($nom): void
    {
        if (is_string($nom) && strlen($nom) <= 30)
        {
            $this->_nom = $nom;
        }
    }

    public function setPrenom($prenom): void
    {
        if (is_string($prenom) && strlen($prenom) <= 30) {
            $this->_prenom = $prenom;
        }
    }

    public function setPasswd($passwd): void
    {
        if (is_string($passwd) && strlen($passwd) <= 30) {
            $this->_passwd = $passwd;
        }
    }

# participer a un cours



# partir d'un cours


}