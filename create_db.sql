create table ENFANT(
id_enfant SERIAL primary key,
nom varchar(20),
prenom varchar(20),
passwd varchar(40));


create table COURS(
id_cours SERIAL primary key,
jour int,
heure_deb time,
heure_fin time);

create table PARTICIPE(
id_enfant SERIAL references ENFANT (id_enfant),
id_cours SERIAL references COURS (id_cours),
PRIMARY KEY(id_enfant, id_cours));



create table PARENT(
id_parent SERIAL primary key,
jour varchar(60),
ratio int,
nom varchar(20),
prenom varchar(20),
passwd varchar(40));


create table FAMILLE(
id_enfant SERIAL references ENFANT (id_enfant),
id_parent SERIAL references PARENT (id_parent),
PRIMARY KEY (id_enfant, id_parent));
